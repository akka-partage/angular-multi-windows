import { HostListener, Injectable, OnInit } from "@angular/core";
import { BehaviorSubject, Observable, observable, Subject } from "rxjs";

declare const window: any;

export interface ScreenDetails {
  width: number;
  height: number;
  left: number;
  top: number;
}

export interface MessageScreenTypes {
  emetteur: string;
  message: string;
}

@Injectable({
  providedIn: "root",
})
export class ScreenService {
  interval: any;

  mainSubject = new BehaviorSubject<string>("");
  mapSubject = new BehaviorSubject<string>("");

  constructor() {
    localStorage.clear();
    this.interval = setInterval(() => {
      this.readMapMessage();
      this.readMainMessage();
    }, 200);
  }

  openWindow(url: string, name: string, width: number = 600, height: number = 400) {
    let left = window.screen.availLeft + width / 2;
    let top = window.screen.availTop + (window.screen.availHeight - height) / 2;
    let features = `width=${width},height=${height},left=${left},top=${top}`;
    window.open(url, name, features);
  }

  mapWindowSendMessage(message: string) {
    localStorage.setItem("mapWindow-message", message);
  }

  mainWindowSendMessage(message: string) {
    localStorage.setItem("mainWindow-message", message);
  }

  readMainMessage() {
    const message = localStorage.getItem("mainWindow-message");
    if (message !== this.mainSubject.value) {
      this.mainSubject.next(message ?? "");
    }
  }

  readMapMessage() {
    const message = localStorage.getItem("mapWindow-message");
    if (message !== this.mapSubject.value) {
      this.mapSubject.next(message ?? "");
    }
  }

  ngOnDestroy() {
    console.log("ngOnDestroy: cleaning up...");
    clearInterval(this.interval);
  }
}
