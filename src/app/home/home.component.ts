import { Component, HostListener, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { MessageScreenTypes, ScreenService } from "../screen.service";

declare const window: any;
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnDestroy {
  receivedMessage: string[] = [];
  mapWindow: any;
  constructor(private screenService: ScreenService) {
    this.screenService.openWindow(window.location.href + "map", "Map");
    this.screenService.mapSubject.subscribe((message: string) => {
      this.receivedMessage = [message, ...this.receivedMessage].slice(0, 10);
    });
  }

  sendMessage() {
    const input = document.getElementById("input-message") as HTMLInputElement;
    if (input) {
      this.screenService.mainWindowSendMessage(input.value);
    }
  }

  ngOnDestroy(): void {}
}
