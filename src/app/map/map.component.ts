import { Component, HostListener, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { MessageScreenTypes, ScreenService } from "../screen.service";

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.scss"],
})
export class MapComponent implements OnDestroy {
  receivedMessage: string[] = [];

  constructor(private screenService: ScreenService) {
    this.screenService.mainSubject.subscribe((message: string) => {
      this.receivedMessage = [message, ...this.receivedMessage].slice(0, 10);
    });
  }

  sendMessage() {
    const input = document.getElementById("input-message") as HTMLInputElement;
    if (input) {
      this.screenService.mapWindowSendMessage(input.value);
    }
  }

  refresh() {}

  ngOnDestroy(): void {}
}
