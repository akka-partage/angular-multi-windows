import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { MapComponent } from "./map/map.component";
import { ScreenService } from "./screen.service";

@NgModule({
  declarations: [AppComponent, HomeComponent, MapComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [ScreenService],
  bootstrap: [AppComponent],
})
export class AppModule {}
